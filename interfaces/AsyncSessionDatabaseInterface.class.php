<?php

interface AsyncSessionDatabaseInterface {
	
	/**
	 * @since 1.0.0
	 * 
	 * @param array $params An array of the parameters to be used for configuring this database mechanism.
	 */
	public function configure( $params );
	
	/**
	 * @since 1.0.0
	 * 
	 * This function will be called to initialize the database mechanism prior to being used for the first time. It should set up the database connections/dependencies/etc. When this function returns, the database mechanism should be ready to get/set/delete/etc.
	 */
	public function init();
	
	/**
	 * This function should return the total amount of time spent doing read/writes in seconds.
	 *
	 * @since 1.0.0
	 *
	 * @return float The total amount of time spent reading/writing in seconds.
	 */
	public function getRuntime();
	
	/**
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value we're attempting to retrieve from the database.
	 * @return mixed The value found, or boolean false if the value could not be found.
	 */
	public function get( $sid , $name );
	
	/**
	 * 
	 * This function will attempt to load multiple values from the database. It may return a partial result.
	 * If you request 4 values, but only 1 is found, you will receive an array with 1 key/value pair in it where the key is the one $names item that was found and the value is its value.
	 * 
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param array $names The names of the values we're attempting to retrieve from the database.
	 * @return array An associative array of the values found, or boolean false on error.
	 */
	public function getMulti( $sid , $names );
	
	/**
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value we're saving in the database.
	 * @param mixed $value The value to store in the database.  
	 * @return boolean True on success, false otherwise.
	 */
	public function set( $sid , $name , $value );
	
	/**
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param array $name An array of key/value pairs to store in the database.
	 * @return boolean True on success, false otherwise.
	 */
	public function setMulti( $sid , $items );
	
	/**
	 * This function is identical to `set` with one exception; it will fail and return false if $name already exists.
	 *
	 * @since 1.0.0
	 *
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value we're saving in the database.
	 * @param mixed $value The value to store in the database.
	 * @return boolean True on success, false otherwise.
	 */
	public function add( $sid , $name , $value );
	
	/**
	 * 
	 * This function is identical to `set` with one exception; it will fail and return false if $name doesn't already exist.
	 * 
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value we're saving in the database.
	 * @param mixed $value The value to store in the database.  
	 * @return boolean True on success, false otherwise.
	 */
	public function replace( $sid , $name , $value );
	
	/**
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value we're deleting from the database.
	 * @return boolean True if the delete succeeded, false otherwise.
	 */
	public function delete( $sid , $name );
	
	/**
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param array $name An array of the names of the values we're deleting from the database.
	 * @return boolean True if the delete succeeded, false otherwise.
	 */
	public function deleteMulti( $sid , $names );
	
	/**
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value to increment.
	 * @param number $amount The amount to add to the value.
	 * @return mixed The new value on success, or boolean false on failure.
	 */
	public function increment( $sid , $name , $amount = 1 , $initial_value = 0 );
	
	/**
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value to decrement.
	 * @param number $amount The amount to subtract from the value.
	 * @return mixed The new value on success, or boolean false on failure.
	 */
	public function decrement( $sid , $name , $amount = 1 , $initial_value = 0 );
	
	/**
	 * @param string $sid The session id this operation is for.
	 * @param string $scriptId The id of this script.
	 * @param string $name The name of the value we're attempting to lock.
	 * @return boolean Returns true if the lock was acquired or false on failure/timeout.
	 */
	public function acquireLock( $sid , $scriptId , $name );
	
	/**
	 * @param string $sid The session id this operation is for.
	 * @param string $scriptId The id of this script.
	 * @param string $name The name of the value we're attempting to lock.
	 * @return boolean Returns true if the lock was released, false if there was an error.
	*/
	public function releaseLock( $sid , $scriptId , $name );
	
}