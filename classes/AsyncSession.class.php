<?php

if( !class_exists( 'AsyncSession') ) {
	
	class AsyncSession {
		
		/**
		 * @var AsyncSession Holds the singleton instance of AsyncSession for this script.
		 */
		protected static $_i = NULL;
		
		/**
		 * @since 1.0.0
		 * 
		 * @var array Holds the current configuration.
		 */
		protected static $_configuration = array();
		
		/**
		 * @since 1.0.0
		 * 
		 * @var array
		 */
		protected static $_defaultConfiguration = array(
			'cache' => 'memory',
			'database' => 'mysql',
			'mysql_config' => array(
				'mysql_server' => array(
					'host' => 'localhost',
					'port' => '3306',
					'user' => 'root',
					'password' => '',
					'database' => 'asyncsession',
					'table' => 'session'
				)
			),
			'memcached_config' => array(
				'memcached_servers' => array(
					array(
						'host' => 'localhost',
						'port' => 11211,
						'weight' => 0
					)
				)
			),
		);
		
		/**
		 * @since 1.0.0
		 * 
		 * @var AsyncSessionCacheInterface An instance of a cache mechanism that implements the AsyncSessionCacheInterface.
		 */
		protected $_cache;
		
		/**
		 * @since 1.0.0
		 * 
		 * @var AsyncSessionDatabaseInterface An instance of a database mechanism that implements the AsyncSessionDatabaseInterface.
		 */
		protected $_database;
		
		/**
		 * @since 1.0.0
		 * 
		 * @return AsyncSession Returns the singleton instance of AsyncSession for this script.
		 */
		public static function &getInstance() {
			if( self::$_i === NULL ) self::$_i = new self();
			return self::$_i;
		}
		
		/**
		 * This must be called before you can use `getInstance`
		 * 
		 * @since 1.0.0
		 * 
		 * @param array $params The configuration parameters for AsyncSession.
		 */
		public static function configure( $params = array() ) {
			self::$_configuration = array_merge( self::$_defaultConfiguration , $params );
		}
		
		/**
		 * @since 1.0.0
		 * 
		 * @return string Creates and returns a 64-character hexadecimal string seeded with microtime() and a random 32-bit integer to be used as a UUID.
		 */
		public static function generateUUID() {
			return md5( rand( 0 , pow( 2 , 32 ) ) ).md5( microtime() );
		}
		
		/**
		 * This function is registered as a shutdown function in PHP to free any locks that may have been left unclosed somehow. Usually this can only occur if the user called getForUpdate without making an update.
		 */
		public static function _shutdown() {
			if( self::$_i ) {
				self::$_i->_releaseAllLocks();
			}
		}
		
		/*
		 * Instance properties
		 */
		
		/**
		 * @var string The session id for this instance.
		 */
		protected $_sid;
		
		/**
		 * @var string A UUID identifying this script.
		 */
		protected $_scriptId;
		
		/**
		 * @var array An array of the locks acquired by this script.
		 */
		protected $_locks = array();
		
		/*
		 * Instance methods
		 */
		
		/**
		 * Do not create direct instances of this object. Use the static `getInstance` method instead.
		 *
		 * @since 1.0.0
		 *
		 * The construct should read the existing configuration and instance the appropriate cache/database handlers.
		 */
		public function __construct() {
			
			//register shutdown function
			register_shutdown_function( array( 'AsyncSession' , '_shutdown' ) );
			
			//if config isn't set up yet, error out
			if( empty( self::$_configuration ) ) trigger_error( 'You must call AsyncSession::configure() before you can create an instance of the session object.' , E_USER_ERROR );

			//set the script id
			$this->_scriptId = self::generateUUID();
			
			//load the cache controller
			switch( self::$_configuration['cache'] ) {
				case 'memcached':
					require_once( 'AsyncSessionMemcachedCache.class.php' );
					$this->_cache = new AsyncSessionMemcachedCache();
					$this->_cache->configure( self::$_configuration['memcached_config'] );
					$this->_cache->init();
					break;
				default:
					require_once( 'AsyncSessionMemoryCache.class.php' );
					$this->_cache = new AsyncSessionMemoryCache();
					//memory cache has no config to setup
					//memory cache has no init to run
			}
			
			switch( self::$_configuration['database'] ) {
				case 'mysql':
					require_once( 'AsyncSessionMysqlDatabase.class.php' );
					$this->_database = new AsyncSessionMysqlDatabase();
					$this->_database->configure( self::$_configuration['mysql_config'] );
					$this->_database->init();
					break;
			}
				
		}
		
		/**
		 * @since 1.0.0
		 * 
		 * @param string $name The name of the value you wish to get a lock for.
		 */
		protected function _acquireLock( $name ) {
			
			//see if we already have a lock on this and return true if we do
			if( isset( $this->_locks[ $name ] ) ) return true;
			
			$lockAcquired = $this->_database->acquireLock( $this->_sid , $this->_scriptId , $name );
			if( !$lockAcquired ) {
				trigger_error( "Failed to acquire lock for session id `$this->_sid` for script id `$this->_scriptId` on field `$name`." , E_USER_WARNING );
				return false;
			}
			
			$this->_locks[ $name ] = true;
			
			return true;
		}
		
		/**
		 * @since 1.0.0
		 * 
		 * @param string $name The name of the value you wish to get a lock for.
		 */
		protected function _releaseLock( $name ) {
			
			//if we don't have a lock on this, return false
			if( !isset($this->_locks[ $name ]) ) return false;
			
			$lockReleased = $this->_database->releaseLock( $this->_sid , $this->_scriptId , $name );
			if( !$lockReleased ) {
				trigger_error( "Failed to release lock for session id `$this->_sid` for script id `$this->_scriptId` on field `$name`." , E_USER_WARNING );
				return false;
			}
			
			unset( $this->_locks[ $name ] );
			
			return true;
		}
		
		/**
		 * This functions releases all locks.
		 */
		protected function _releaseAllLocks() {
			$locks = array_keys( $this->_locks );
			foreach( $locks as $name ) {
				$this->_releaseLock($name);
			}
		}
		
		/**
		 * @since 1.0.0
		 * 
		 * @param string $sid Sets the session id for this instance.
		 */
		public function setSid( $sid ) {
			$this->_sid = $sid;
		}
		
		/**
		 * Get the session id.
		 * 
		 * @since 1.0.0
		 * 
		 * @return string The session id for this instance.
		 */
		public function getSid() {
			return $this->_sid;
		}
		
		/**
		 * @param mixed $params An array, string, etc of data to use to set the authorization token for this session.
		 * @return boolean True if the params were saved, false otherwise.
		 */
		public function setAuth( $params ) {
			return $this->set('__auth__', sha1( serialize($params) ) );
		}
		
		/**
		 * @param mixed $params An array, string, etc of data to use to set the authorization token for this session. This must EXACTLY match the data passed to `setAuth` or it will return false. This means in an array, the order of elements DOES matter.
		 * @return boolean True if the authorization matches, false if it does not.
		 */
		public function checkAuth( $params ) {
			$auth = $this->get( '__auth__' );
			//if no auth was set, then anyone can use this session
			if( !$auth ) return true;
			//if auth matches, return true
			if( sha1( serialize($params) ) == $auth ) return true;
			//the auth didn't match
			return false;
		}
		
		/**
		 * Get a value from the session.
		 *
		 * @param string $name The name of the value to get from the session.
		 * @return mixed The value or NULL if the value is unset.
		 */
		public function get( $name ) {
				
			$_return = $this->_cache->get( $this->_sid , $name );
			if( $_return ) return $_return;
			
			//if we're here, the cache lookup failed so we need to load from the database
			$_return = $this->_database->get( $this->_sid , $name );
			
			//if we still have no return value, return null as this is unset
			if( $_return === false ) return NULL;
			
			//otherwise, push this into the cache
			$this->_cache->set( $this->_sid , $name , $_return );
			
			return $_return;
				
		}
		
		/**
		 * Get a value from the session, but lock that value for updating.
		 *
		 * @param string $name The name of the value to get from the session.
		 * @return mixed Returns false if the lock could not be acquired. Otherwise, returns the value or NULL if the value is unset.
		 */
		public function getForUpdate( $name ) {
			
			if( !$this->_acquireLock($name) ) return false;
			
			$_return = $this->_cache->get( $this->_sid , $name );
			if( $_return ) return $_return;
				
			//if we're here, the cache lookup failed so we need to load from the database
			$_return = $this->_database->get( $this->_sid , $name );
			
			//if we still have no return value, return null as this is unset
			if( $_return === false ) return NULL;
			
			//otherwise, push this into the cache
			$this->_cache->set( $this->_sid , $name , $_return );
		
			return $_return === false ? NULL : $_return;
		
		}
		
		/**
		 * Set a value in the session.
		 * 
		 * @param string $name The name of the value to set in the session.
		 * @param mixed $value The value to set.
		 * @return boolean True for success, false otherwise.
		 */
		public function set( $name , $value ) {
			
			if( !$this->_acquireLock($name) ) return false;
			
			$dbResult = $this->_database->set( $this->_sid , $name , $value );
			$this->_cache->set( $this->_sid , $name , $value );
			
			$this->_releaseLock($name);
			
			return $dbResult;
			
		}
		
		/**
		 * Delete a value from the session.
		 * 
		 * @since 1.0.0
		 * 
		 * @param string $name The name of the value to delete from the session.
		 * @return boolean True for success, false otherwise.
		 */
		public function delete( $name ) {
				
			if( !$this->_acquireLock($name) ) return false;
				
			$this->_cache->delete( $this->_sid , $name );
			$dbResult = $this->_database->delete($this->_sid, $name);
				
			$this->_releaseLock($name);
				
			return $dbResult;
				
		}
		
		/**
		 * Increment a value in the session.
		 *
		 * @param string $name The name of the value to increment in the session.
		 * @param mixed $value The amount to increment by.
		 * @return boolean True for success, false otherwise.
		 */
		public function increment( $name , $amount = 1 ) {
				
			if( !$this->_acquireLock($name) ) return false;
				
			$this->_cache->increment( $this->_sid , $name , $amount );
			$dbResult = $this->_database->increment( $this->_sid , $name , $amount );
				
			$this->_releaseLock($name);
				
			return $dbResult;
				
		}
		
		/**
		 * Decrement a value in the session.
		 *
		 * @param string $name The name of the value to decrement in the session.
		 * @param mixed $value The amount to increment by.
		 * @return boolean True for success, false otherwise.
		 */
		public function decrement( $name , $amount = 1 ) {
		
			if( !$this->_acquireLock($name) ) return false;
		
			$this->_cache->decrement( $this->_sid , $name , $amount );
			$dbResult = $this->_database->decrement( $this->_sid , $name , $amount );
		
			$this->_releaseLock($name);
		
			return $dbResult;
		
		}
		
		/**
		 * @return float The total runtime of the cache operations in seconds.
		 */
		public function getCacheRuntime() {
			return $this->_cache->getRuntime();
		}
		
		/**
		 * @return float The total runtime of the database operations in seconds.
		 */
		public function getDatabaseRuntime() {
			return $this->_database->getRuntime();
		}
		
	}
	
}