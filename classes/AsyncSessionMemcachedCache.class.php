<?php

class AsyncSessionMemcachedCache implements AsyncSessionCacheInterface {
	
	/**
	 * @since 1.0.0
	 * 
	 * @var array The configuration for this instance.
	 */
	protected $_configuration = array();
	
	/**
	 * @var array The default configuration options.
	 */
	protected $_defaultConfiguration = array(
		'memcached_servers' => array(
			array(
				'host' => 'localhost',
				'port' => 11211,
				'weight' => 0
			)
		)
	);
	
	/**
	 * @var Memcached The Memcached instance for this cache instance.
	 */
	protected $_memcached;
	
	/**
	 * @var float Holds the total runtime in seconds.
	 */
	protected $_runtime = 0;
	
	/**
	 * @var float Holds the start time of the last operation with microseconds as a float.
	 */
	protected $_clockStart = 0;
	
	/**
	 * @since 1.0.0
	 * 
	 * @param array $params An array of the parameters to be used for configuring this cache mechanism.
	 */
	public function configure( $params ) {
		$this->_configuration = array_merge( $this->_defaultConfiguration , $params );
	}
	
	/**
	 * @since 1.0.0
	 * 
	 * This function will be called to initialize the cache mechanism prior to being used for the first time. It should set up the cache connections/dependencies/etc. When this function returns, the cache mechanism should be ready to get/set/delete/etc.
	 */
	public function init() {
		//check configuration
		if( empty( $this->_configuration['memcached_servers'] ) ) trigger_error( 'You must set at least one Memcached server in the AsyncSession configuration under the "memcached_servers" property.' , E_USER_ERROR );
		$this->_memcached = new Memcached();
		$this->_memcached->addServers( $this->_configuration['memcached_servers'] );
	}
	
	/**
	 * This function should return the total amount of time spent doing read/writes in seconds.
	 *
	 * @since 1.0.0
	 *
	 * @return float The total amount of time spent reading/writing in seconds.
	 */
	public function getRuntime() {
		return $this->_runtime;
	}
	
	/**
	 * Called at the start of an operation. Sets the start time for calculations.
	 */
	protected function _beginOperation() {
		$this->_clockStart = microtime(true);
	}
	
	/**
	 * Called at the end of an operation. Adds the runtime to the clock.
	 */
	protected function _endOperation() {
		$this->_runtime += ( microtime(true) - $this->_clockStart );
	}
	
	/**
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value we're attempting to retrieve from the cache.
	 * @return mixed The value found, or boolean false if the value could not be found.
	 */
	public function get( $sid , $name ) {
		$this->_beginOperation();
		$_return = $this->_memcached->get( $sid.$name );
		$this->_endOperation();
		return $_return;
	}
	
	/**
	 * 
	 * This function will attempt to load multiple values from the cache. It may return a partial result.
	 * If you request 4 values, but only 1 is found, you will receive an array with 1 key/value pair in it where the key is the one $names item that was found and the value is its value.
	 * 
	 * @since 1.0.0
	 *
	 * @param string $sid The session id this operation is for.
	 * @param array $names The names of the values we're attempting to retrieve from the cache.
	 * @return array An associative array of the values found, or boolean false on error.
	 */
	public function getMulti( $sid , $names ) {
		$this->_beginOperation();
		foreach( $names as &$name ) {
			$name = $sid.$name;
		}
		$_return = $this->_memcached->getMulti( $names , NULL , Memcached::GET_PRESERVE_ORDER );
		$this->_endOperation();
		return $_return;
	}
	
	/**
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value we're saving in the cache.
	 * @param mixed $value The value to store in the cache.
	 * @param integer $expiration The number of seconds this item can live in the cache before being purged. If this value exceeds 60*60*24*30 it will be treated as a Unix Timestamp meant to represent the exact expiration time of the item.  
	 * @return boolean True on success, false otherwise.
	 */
	public function set( $sid , $name , $value , $expiration = 0 ) {
		$this->_beginOperation();
		$_return = $this->_memcached->set( $sid.$name , $value , $expiration );
		$this->_endOperation();
		return $_return;
	}
	
	/**
	 * @since 1.0.0
	 *
	 * @param string $sid The session id this operation is for.
	 * @param array $name An array of key/value pairs to store in the cache.
	 * @param integer $expiration The number of seconds each of these items can live in the cache before being purged. If this value exceeds 60*60*24*30 it will be treated as a Unix Timestamp meant to represent the exact expiration time of the item.
	 * @return boolean True on success, false otherwise.
	 */
	public function setMulti( $sid , $items , $expiration = 0 ) {
		$this->_beginOperation();
		$_items = array();
		foreach( $items as $name => $value ) {
			$_items[ $sid.$name ] = $value;
		}
		$_return = $this->_memcached->setMulti( $items , $expiration );
		$this->_endOperation();
		return $_return;
	}
	
	/**
	 * This function is identical to `set` with one exception; it will fail and return false if $name already exists.
	 * 
	 * @since 1.0.0
	 *
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value we're saving in the cache.
	 * @param mixed $value The value to store in the cache.
	 * @param integer $expiration The number of seconds this item can live in the cache before being purged. If this value exceeds 60*60*24*30 it will be treated as a Unix Timestamp meant to represent the exact expiration time of the item.
	 * @return boolean True on success, false otherwise.
	 */
	public function add( $sid , $name , $value , $expiration = 0 ) {
		$this->_beginOperation();
		$_return = $this->_memcached->add( $sid.$name , $value , $expiration );
		$this->_endOperation();
		return $_return;
	}
	
	/**
	 * This function is identical to `set` with one exception; it will fail and return false if $name doesn't already exist.
	 * 
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value we're saving in the cache.
	 * @param mixed $value The value to store in the cache.
	 * @param integer $expiration The number of seconds this item can live in the cache before being purged. If this value exceeds 60*60*24*30 it will be treated as a Unix Timestamp meant to represent the exact expiration time of the item.  
	 * @return boolean True on success, false otherwise.
	 */
	public function replace( $sid , $name , $value , $expiration = 0 ) {
		$this->_beginOperation();
		$_return = $this->_memcached->replace( $sid.$name , $value , $expiration );
		$this->_endOperation();
		return $_return;
	}
	
	/**
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value we're deleting from the cache.
	 * @return boolean True if the delete succeeded, false otherwise.
	 */
	public function delete( $sid , $name ) {
		$this->_beginOperation();
		$_return = $this->_memcached->delete( $sid.$name );
		$this->_endOperation();
		return $_return;
	}
	
	/**
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param array $name An array of the names of the values we're deleting from the cache.
	 * @return boolean True if the delete succeeded, false otherwise.
	 */
	public function deleteMulti( $sid , $names ) {
		$this->_beginOperation();
		foreach( $names as &$name ) {
			$name = $sid.$name;
		}
		$_return = $this->_memcached->deleteMulti( $names );
		$this->_endOperation();
		return $_return;
	}
	
	/**
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value to increment.
	 * @param number $amount The amount to add to the value.
	 * @param number $initial_value The initial value to set if $name is not set.
	 * @param number $expiration The number of seconds this item can live in the cache before being purged. If this value exceeds 60*60*24*30 it will be treated as a Unix Timestamp meant to represent the exact expiration time of the item.
	 * @return mixed The new value on success, or boolean false on failure.
	 */
	public function increment( $sid , $name , $amount = 1 , $initial_value = 0 , $expiration = 0 ) {
		$this->_beginOperation();
		$_return = $this->_memcached->increment( $sid.$name , $amount , $initial_value , $expiration );
		$this->_endOperation();
		return $_return;
	}
	
	/**
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value to decrement.
	 * @param number $amount The amount to subtract from the value.
	 * @param number $initial_value The initial value to set if $name is not set.
	 * @param number $expiration The number of seconds this item can live in the cache before being purged. If this value exceeds 60*60*24*30 it will be treated as a Unix Timestamp meant to represent the exact expiration time of the item.
	 * @return mixed The new value on success, or boolean false on failure.
	 */
	public function decrement( $sid , $name , $amount = 1 , $initial_value = 0 , $expiration = 0 ) {
		$this->_beginOperation();
		$_return = $this->_memcached->decrement( $sid.$name , $amount , $initial_value , $expiration );
		$this->_endOperation();
		return $_return;
	}
	
	/**
	 * 
	 * This function sets a new expiration on an existing item
	 * 
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value to update.
	 * @param integer $expiration The number of seconds this item can live in the cache before being purged. If this value exceeds 60*60*24*30 it will be treated as a Unix Timestamp meant to represent the exact expiration time of the item.
	 * @return boolean True on success, false on failure or if $name doesn't exist in the cache.
	 */
	public function touch( $sid , $name , $expiration = 0 ) {
		$this->_beginOperation();
		$_return = $this->_memcached->touch( $sid.$name , $expiration );
		$this->_endOperation();
		return $_return;
	}
	
}