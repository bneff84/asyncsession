<?php

class AsyncSessionMemoryCache implements AsyncSessionCacheInterface {
	
	private $_maxExpiration = 2592000;
	
	private $_cache = array();
	private $_expirations = array();
	
	/*
	 * Helper methods
	 */
	
	private function _isset( $name ) {
		return isset( $this->_cache[ $name ] );
	}
	
	private function _isExpired( $name ) {
		//is this even set?
		if( !$this->_isset($name) ) return false;
		//does this have an expiration time?
		if( !isset( $this->_expirations[ $name ] ) ) return false;
		//see if the time has passed
		if( $this->_expirations[ $name ] >= time() ) {
			//this is expired, clear it
			$this->delete($name);
			return true;
		}
		return false;
	}
	
	private function _setExpiration( $name , $expiration ) {
		//if expiration is zero, remove any existing expiration time and return
		if( $expiration === 0 ) {
			unset( $this->_expirations[ $name ] );
			return;
		}
		if( $expiration > $this->_maxExpiration ) {
			//treat this as a unix timestamp
			$this->_expirations[ $name ] = $expiration;
		} else {
			//treat this as seconds into the future
			$this->_expirations[ $name ] = time() + $expiration;
		}
	}
	
	/*
	 * End Helper Methods
	 */
	
	public function add( $sid , $name , $value , $expiration = 0 ) {
		
		//if it's set already, check expiration
		if( $this->_isset($name) ){
			//if it is not expired, return false. otherwise continue since it's now unset (expired)
			if( !$this->_isExpired( $name ) ) return false;
		}
		
		//cache the value
		$this->_cache[ $name ] = $value;
		
		//set the expiration
		$this->_setExpiration($name, $expiration);
		
		return true;
		
	}
	
	public function replace( $sid , $name , $value , $expiration = 0 ) {
		
		//if it's not set, return false
		if( !$this->_isset($name) ) return false;
		
		//if it is expired, return false
		if( $this->_isExpired($name) ) return false;
		
		//cache the value
		$this->_cache[ $name ] = $value;
		
		//set the expiration
		$this->_setExpiration($name, $expiration);
		
		return true;
		
	}

	public function increment( $sid , $name , $amount = 1 , $initial_value = 0 , $expiration = 0 ) {
		
		//if it's not set or if it is set and is expired, set the default value and return
		if( !$this->_isset($name) || $this->_isExpired($name) ) {
			$this->_cache[ $name ] = $initial_value;
			$this->_setExpiration($name, $expiration);
			return true;
		}
		
		//if it's not numeric, return false
		if( !is_numeric( $this->_cache[ $name ] ) ) return false;
		
		$this->_cache[ $name ] += $amount;
		
		return true;
		
	}

	public function getMulti( $sid , $names ) {
		
		$_return = array();
		
		foreach( $names as $name ) {
			//if it's not set, continue
			if( !$this->_isset($name) ) continue;
			
			//if it is expired, continue
			if( $this->_isExpired($name) ) continue;
			
			$_return[] = $this->_cache[ $name ];
		}
		
		return $_return;
		
	}

	public function flush( $sid ) {
		$this->_cache = array();
		return true;
	}

	public function touch( $sid , $name , $expiration = 0 ) {
		//if it's not set, return false
		if( !$this->_isset($name) ) return false;
		
		//if it is expired, return false
		if( $this->_isExpired($name) ) return false;
		
		$this->_setExpiration($name, $expiration);
		
		return true;
	}

	public function get( $sid , $name ) {
		//if it's not set, return false
		if( !$this->_isset($name) ) return false;
		
		//if it is expired, return false
		if( $this->_isExpired($name) ) return false;
		
		return $this->_cache[ $name ];
	}

	public function deleteMulti( $sid , $names ) {
		foreach( $names as $name ) {
			unset( $this->_cache[ $name ] , $this->_expirations[ $name ] );
		}
		return true;
	}

	public function set( $sid , $name , $value , $expiration = 0 ) {
		//cache the value
		$this->_cache[ $name ] = $value;
		
		//set the expiration
		$this->_setExpiration($name, $expiration);
		
		return true;
	}

	public function delete( $sid , $name ) {
		unset( $this->_cache[ $name ] , $this->_expirations[ $name ] );
	}

	public function init() {
		//no setup required
	}

	public function setMulti( $sid , $items , $expiration = 0 ) {
		foreach( $items as $name => $value ) {
			//cache the value
			$this->_cache[ $name ] = $value;
			
			//set the expiration
			$this->_setExpiration($name, $expiration);
		}
		return true;
	}

	public function configure( $params ) {
		//this doesn't have any params
	}

	public function decrement( $sid , $name , $amount = 1 , $initial_value = 0 , $expiration = 0 ) {
		
		//if it's not set or if it is set and is expired, set the default value and return
		if( !$this->_isset($name) || $this->_isExpired($name) ) {
			$this->_cache[ $name ] = $initial_value;
			$this->_setExpiration($name, $expiration);
			return true;
		}
		
		//if it's not numeric, return false
		if( !is_numeric( $this->_cache[ $name ] ) ) return false;
		
		$this->_cache[ $name ] -= $amount;
		
		return true;
	}
	
	public function getRuntime() {
		return 0;
	}
	
}

?>