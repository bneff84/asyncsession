<?php

class AsyncSessionMysqlDatabase implements AsyncSessionDatabaseInterface {
	
	/**
	 * @since 1.0.0
	 *
	 * @var array The configuration for this instance.
	 */
	protected $_configuration = array();
	
	/**
	 * @var array The default configuration options.
	*/
	protected $_defaultConfiguration = array(
		'mysql_server' => array(
			'host' => 'localhost',
			'port' => '3306',
			'user' => 'root',
			'password' => '',
			'database' => 'asyncsession',
			'table' => 'session',
		)
	);
	
	/**
	 * @var mysqli The instance of mysqli for this database interface.
	 */
	protected $_mysqli;
	
	/**
	 * @var array An array of the locks acquired by this database instance.
	 */
	protected $_locks = array();
	
	/**
	 * @var string The name of the database table, so we don't have to reference the config every time.
	 */
	protected $_table;
	
	/**
	 * @var float Holds the total runtime in seconds.
	 */
	protected $_runtime = 0;
	
	/**
	 * @var float Holds the start time of the last operation with microseconds as a float.
	 */
	protected $_clockStart = 0;
	
	/**
	 * @param mixed $value The value being inserted into the database.
	 * @return mixed The value ready to be inserted into a SQL query.
	 */
	protected function _prepareValue( $value ) {
		$_return = $value;
		
		//if it's an array or object, serialize it
		if ( is_array( $_return ) || is_object( $_return ) )
			$_return = serialize( $_return );
		
		//escape it for db insertion
		return $this->_mysqli->escape_string($_return);
	}
	
	protected function _unprepare( $value ) {
		if ( $this->_is_serialized( $value ) ) // don't attempt to unserialize data that wasn't serialized going in
			return @unserialize( $value );
		return $value;
	}
	
	/**
	 * (Ganked from Wordpress)
	 * Check value to find if it was serialized.
	 *
	 * If $data is not an string, then returned value will always be false.
	 * Serialized data is always a string.
	 *
	 * @since 1.0.0
	 *
	 * @param mixed $data Value to check to see if was serialized.
	 * @param bool $strict Optional. Whether to be strict about the end of the string. Defaults true.
	 * @return bool False if not serialized and true if it was.
	 */
	protected function _is_serialized( $data, $strict = true ) {
		// if it isn't a string, it isn't serialized
		if ( ! is_string( $data ) ) {
			return false;
		}
		$data = trim( $data );
		if ( 'N;' == $data ) {
			return true;
		}
		if ( strlen( $data ) < 4 ) {
			return false;
		}
		if ( ':' !== $data[1] ) {
			return false;
		}
		if ( $strict ) {
			$lastc = substr( $data, -1 );
			if ( ';' !== $lastc && '}' !== $lastc ) {
				return false;
			}
		} else {
			$semicolon = strpos( $data, ';' );
			$brace     = strpos( $data, '}' );
			// Either ; or } must exist.
			if ( false === $semicolon && false === $brace )
				return false;
			// But neither must be in the first X characters.
			if ( false !== $semicolon && $semicolon < 3 )
				return false;
			if ( false !== $brace && $brace < 4 )
				return false;
		}
		$token = $data[0];
		switch ( $token ) {
			case 's' :
				if ( $strict ) {
					if ( '"' !== substr( $data, -2, 1 ) ) {
						return false;
					}
				} elseif ( false === strpos( $data, '"' ) ) {
					return false;
				}
				// or else fall through
			case 'a' :
			case 'O' :
				return (bool) preg_match( "/^{$token}:[0-9]+:/s", $data );
			case 'b' :
			case 'i' :
			case 'd' :
				$end = $strict ? '$' : '';
				return (bool) preg_match( "/^{$token}:[0-9.E-]+;$end/", $data );
		}
		return false;
	}
	
	/*
	 * Implementation methods
	 */
	
	/**
	 * @since 1.0.0
	 * 
	 * @param array $params An array of the parameters to be used for configuring this database mechanism.
	 */
	public function configure( $params ) {
		$this->_configuration = array_merge( $this->_defaultConfiguration , $params );
		//alias the database table
		$this->_table =& $this->_configuration['mysql_server']['table'];
	}
	
	/**
	 * @since 1.0.0
	 * 
	 * This function will be called to initialize the database mechanism prior to being used for the first time. It should set up the database connections/dependencies/etc. When this function returns, the database mechanism should be ready to get/set/delete/etc.
	 */
	public function init() {
		$this->_mysqli = new mysqli( $this->_configuration['mysql_server']['host'] , $this->_configuration['mysql_server']['user'] , $this->_configuration['mysql_server']['password'] , $this->_configuration['mysql_server']['database'] , $this->_configuration['mysql_server']['port'] );
		if( $this->_mysqli->connect_errno ) {
			trigger_error( "Failed to connect to MySQL: (" . $this->_mysqli->connect_errno . ") " . $this->_mysqli->connect_error , E_USER_ERROR );
			return;
		}
	}
	
	/**
	 * This function should return the total amount of time spent doing read/writes in seconds.
	 *
	 * @since 1.0.0
	 *
	 * @return float The total amount of time spent reading/writing in seconds.
	 */
	public function getRuntime() {
		return $this->_runtime;
	}
	
	/**
	 * Called at the start of an operation. Sets the start time for calculations.
	 */
	protected function _beginOperation() {
		$this->_clockStart = microtime(true);
	}
	
	/**
	 * Called at the end of an operation. Adds the runtime to the clock.
	 */
	protected function _endOperation() {
		$this->_runtime += ( microtime(true) - $this->_clockStart );
	}
	
	/**
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value we're attempting to retrieve from the database.
	 * @return mixed The value found, or boolean false if the value could not be found.
	 */
	public function get( $sid , $name ) {
		$this->_beginOperation();
		
		$results = $this->_mysqli->query("SELECT `value` FROM `{$this->_table}` WHERE `session_id` = '".$this->_prepareValue($sid)."' AND `name` = '".$this->_prepareValue($name)."' LIMIT 1");
		
		if( !$results || !$results->num_rows ) {
			return false;
		}
		
		//grab the row
		$row = $results->fetch_object();
		//get the value
		$_return = $this->_unprepare( $row->value );
		
		$this->_endOperation();
		return $_return;
	}
	
	/**
	 * 
	 * This function will attempt to load multiple values from the database. It may return a partial result.
	 * If you request 4 values, but only 1 is found, you will receive an array with 1 key/value pair in it where the key is the one $names item that was found and the value is its value.
	 * 
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param array $names The names of the values we're attempting to retrieve from the database.
	 * @return array An associative array of the values found, or boolean false on error.
	 */
	public function getMulti( $sid , $names ) {
		$this->_beginOperation();
		
		//escape the names
		foreach( $names as &$name ) {
			$name = $this->_prepareValue($name);
		}
		
		$results = $this->_mysqli->query("SELECT `value` FROM `{$this->_table}` WHERE `session_id` = '".$this->_prepareValue($sid)."' AND ( `name` = '".implode("' OR `name` = '",$names)."' ) LIMIT");
		
		if( !$results || !$results->num_rows ) {
			return false;
		}
		
		//grab the rows
		$_return = array();
		while( $row = $results->fetch_object() ) {
			//get the value
			$_return[] = $this->_unprepare( $row->value );
		}
		
		$this->_endOperation();
		return $_return;
	}
	
	/**
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value we're saving in the database.
	 * @param mixed $value The value to store in the database.  
	 * @return boolean True on success, false otherwise.
	 */
	public function set( $sid , $name , $value ) {
		$this->_beginOperation();
		
		$this->_mysqli->query("UPDATE `{$this->_table}` SET `value` = '".$this->_prepareValue($value)."' WHERE `session_id` = '".$this->_prepareValue($sid)."' AND `name` = '".$this->_prepareValue($name)."' LIMIT 1");
		
		if( $this->_mysqli->affected_rows ) {
			$this->_endOperation();
			return true;
		}
		
		$this->_endOperation();
		return false;
	}
	
	/**
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param array $name An array of key/value pairs to store in the database.
	 * @return boolean True on success, false otherwise.
	 */
	public function setMulti( $sid , $items ) {
		
	}
	
	/**
	 * This function is identical to `set` with one exception; it will fail and return false if $name already exists.
	 *
	 * @since 1.0.0
	 *
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value we're saving in the database.
	 * @param mixed $value The value to store in the database.
	 * @return boolean True on success, false otherwise.
	 */
	public function add( $sid , $name , $value ) {
		$this->_beginOperation();
		
		//see if the value is empty
		$result = $this->_mysqli->query( "SELECT `value` FROM `{$this->_table}` WHERE `session_id` = '".$this->_prepareValue($sid)."' AND `name` = '".$this->_prepareValue($name)."' AND `value` IS NULL LIMIT 1" );
		
		//if we didn't get a result then there is a value for this and it's not NULL, return false as we're not adding a new value
		if( !$result || $result->num_rows == 0 ) {
			$this->_endOperation();
			return false;
		}
		
		$this->_mysqli->query("UPDATE `{$this->_table}` SET `value` = '".$this->_prepareValue($value)."' WHERE `session_id` = '".$this->_prepareValue($sid)."' AND `name` = '".$this->_prepareValue($name)."' LIMIT 1");
		
		if( $this->_mysqli->affected_rows ) {
			$this->_endOperation();
			return true;
		}
		
		$this->_endOperation();
		return false;
	}
	
	/**
	 * 
	 * This function is identical to `set` with one exception; it will fail and return false if $name doesn't already exist.
	 * 
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value we're saving in the database.
	 * @param mixed $value The value to store in the database.  
	 * @return boolean True on success, false otherwise.
	 */
	public function replace( $sid , $name , $value ) {
		$this->_beginOperation();
		
		//see if the value is empty
		$result = $this->_mysqli->query( "SELECT `value` FROM `{$this->_table}` WHERE `session_id` = '".$this->_prepareValue($sid)."' AND `name` = '".$this->_prepareValue($name)."' AND `value` IS NULL LIMIT 1" );
		
		//if we got a result then there is not value for this and we're not replacing it, return false as we're not replacing a value
		if( $result && $result->num_rows > 0 ) {
			$this->_endOperation();
			return false;
		}
		
		$this->_mysqli->query("UPDATE `{$this->_table}` SET `value` = '".$this->_prepareValue($value)."' WHERE `session_id` = '".$this->_prepareValue($sid)."' AND `name` = '".$this->_prepareValue($name)."' LIMIT 1");
		
		if( $this->_mysqli->affected_rows ) {
			$this->_endOperation();
			return true;
		}
		
		$this->_endOperation();
		return false;
	}
	
	/**
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value we're deleting from the database.
	 * @return boolean True if the delete succeeded, false otherwise.
	 */
	public function delete( $sid , $name ) {
		$this->_beginOperation();
		
		//see if the value is empty
		$result = $this->_mysqli->query( "SELECT `value` FROM `{$this->_table}` WHERE `session_id` = '".$this->_prepareValue($sid)."' AND `name` = '".$this->_prepareValue($name)."' AND `value` IS NULL LIMIT 1" );
		
		//if we got a result then there is not value for this and we're not deleting it, return false as we're not deleting a value
		if( $result && $result->num_rows > 0 ) {
			$this->_endOperation();
			return false;
		}
		
		$this->_mysqli->query("UPDATE `{$this->_table}` SET `value` = NULL WHERE `session_id` = '".$this->_prepareValue($sid)."' AND `name` = '".$this->_prepareValue($name)."' LIMIT 1");
		
		if( $this->_mysqli->affected_rows ) {
			$this->_endOperation();
			return true;
		}
		
		$this->_endOperation();
		return false;
	}
	
	/**
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param array $name An array of the names of the values we're deleting from the database.
	 * @return boolean True if the delete succeeded, false otherwise.
	 */
	public function deleteMulti( $sid , $names ) {
		
	}
	
	/**
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value to increment.
	 * @param number $amount The amount to add to the value.
	 * @return mixed The new value on success, or boolean false on failure.
	 */
	public function increment( $sid , $name , $amount = 1 , $initial_value = 0 ) {
		$this->_beginOperation();
		
		//see if the value is empty
		$result = $this->_mysqli->query( "SELECT `value` FROM `{$this->_table}` WHERE `session_id` = '".$this->_prepareValue($sid)."' AND `name` = '".$this->_prepareValue($name)."' AND `value` IS NULL LIMIT 1" );
		
		//if we got a result then there is no value for this, initialize to the default value
		if( $result && $result->num_rows > 0 ) {
			$currentValue = $initial_value;
		} else if( !( $row = $result->fetch_object() ) || !is_numeric( $row->value ) ) {
			//if we're here, then the value is not numeric and we need to return false
			$this->_endOperation();
			return false;
		} else {
			//if we're here then there is a value and it is numeric
			$currentValue = $row->value;
		}
		
		$this->_mysqli->query("UPDATE `{$this->_table}` SET `value` = ".( $currentValue + $amount )." WHERE `session_id` = '".$this->_prepareValue($sid)."' AND `name` = '".$this->_prepareValue($name)."' LIMIT 1");
		
		if( $this->_mysqli->affected_rows ) {
			$this->_endOperation();
			return true;
		}
		
		$this->_endOperation();
		return false;
	}
	
	/**
	 * @since 1.0.0
	 * 
	 * @param string $sid The session id this operation is for.
	 * @param string $name The name of the value to decrement.
	 * @param number $amount The amount to subtract from the value.
	 * @return mixed The new value on success, or boolean false on failure.
	 */
	public function decrement( $sid , $name , $amount = 1 , $initial_value = 0 ) {
		$this->_beginOperation();
		
		//see if the value is empty
		$result = $this->_mysqli->query( "SELECT `value` FROM `{$this->_table}` WHERE `session_id` = '".$this->_prepareValue($sid)."' AND `name` = '".$this->_prepareValue($name)."' AND `value` IS NULL LIMIT 1" );
		
		//if we got a result then there is no value for this, initialize to the default value
		if( $result && $result->num_rows > 0 ) {
			$currentValue = $initial_value;
		} else if( !( $row = $result->fetch_object() ) || !is_numeric( $row->value ) ) {
			//if we're here, then the value is not numeric and we need to return false
			$this->_endOperation();
			return false;
		} else {
			//if we're here then there is a value and it is numeric
			$currentValue = $row->value;
		}
		
		$this->_mysqli->query("UPDATE `{$this->_table}` SET `value` = ".( $currentValue - $amount )." WHERE `session_id` = '".$this->_prepareValue($sid)."' AND `name` = '".$this->_prepareValue($name)."' LIMIT 1");
		
		if( $this->_mysqli->affected_rows ) {
			$this->_endOperation();
			return true;
		}
		
		$this->_endOperation();
		return false;
	}
	
	/**
	 * @param string $sid The session id this operation is for.
	 * @param string $scriptId The id of this script.
	 * @param string $name The name of the value we're attempting to lock.
	 * @return boolean Returns true if the lock was acquired or false on failure/timeout.
	 */
	public function acquireLock( $sid , $scriptId , $name ) {
		
		//if we already have this lock, return true
		if( isset( $this->_locks[ $name ] ) ) return true;
		
		//start operation
		$this->_beginOperation();
		
		//start transaction
		$this->_mysqli->begin_transaction();
		
		//attempt to get a lock on this record
		$sql = "
INSERT IGNORE
	INTO `{$this->_table}` ( `session_id` , `name` , `locked_by` )
	VALUES ( '".$this->_prepareValue($sid)."' , '".$this->_prepareValue($name)."' , '$scriptId' )
ON DUPLICATE KEY UPDATE
	`locked_by` = IF( `locked_by` IS NULL , VALUES( `locked_by` ) , `locked_by` )
";
		/*
		 * This query SHOULD hang until it gets a lock or mysql times out because no script should leave the row unlocked with a locked_by id inside of it.
		 */
		$this->_mysqli->query( $sql );
		if( $this->_mysqli->affected_rows ) {
			//we got a lock with the insert
			$this->_locks[ $name ] = true;
			$this->_endOperation();
			return true;
		}
		
		//if we're here, something managed to leave behind a lock. perhaps a db crash or something occurred, clean this up by attempting to remove the lock
		if( $this->_overrideLock($sid, $scriptId, $name) ) {
			$this->_endOperation();
			return true;
		}
		
		//if we're here, something is seriously fucked up somewhere :X
		$this->_endOperation();
		return false;
		
	}
	
	/**
	 * @param string $sid The session id this operation is for.
	 * @param string $scriptId The id of this script.
	 * @param string $name The name of the value we're attempting to lock.
	 * @return boolean Returns true if the lock was overridden, false if there was an error.
	*/
	protected function _overrideLock( $sid , $scriptId , $name ) {
		
		trigger_error( "Warning: Lock had to be overridden for session ID `$sid` for script `$scriptId` and name `$name`. You should check your code, because something probably crashed and left a lock behind." , E_USER_WARNING );
		
		//start operation
		$this->_beginOperation();
		
		$this->_mysqli->query( "
UPDATE `{$this->_table}`
SET
	`locked_by` = '$scriptId'
WHERE
	`session_id` = '".$this->_prepareValue($sid)."'
	AND `name` = '".$this->_prepareValue($name)."'
LIMIT 1
" );
		
		if( $this->_mysqli->affected_rows ) {
			//override succeeded
			$this->_endOperation();
			return true;
		}
		
		//override failed, let the user know something is really really really really really... well you get the point
		$this->_endOperation();
		trigger_error( "Fatal Error: AsyncSession failed to acquire or override a lock for session `$sid` for script `$scriptId` on field `$name`. You should really check your database, something is probably terribly wrong." , E_USER_ERROR );
		return false;
		
	}
	
	/**
	 * @param string $sid The session id this operation is for.
	 * @param string $scriptId The id of this script.
	 * @param string $name The name of the value we're attempting to lock.
	 * @return boolean Returns true if the lock was released, false if there was an error.
	*/
	public function releaseLock( $sid , $scriptId , $name ) {
		
		//start operation
		$this->_beginOperation();
		
		//free the lock on the database
		$sql = "UPDATE `{$this->_table}` SET `locked_by` = NULL WHERE `session_id` = '".$this->_prepareValue($sid)."' AND `name` = '".$this->_prepareValue($name)."' AND `locked_by` = '$scriptId' LIMIT 1";
		
		$this->_mysqli->query( $sql );
		
		//see if we succeeded in updating
		if( $this->_mysqli->affected_rows ) {
			//clear the internal reference that we have this lock
			unset( $this->_locks[ $name ] );
			
			//commit the transaction
			$this->_mysqli->commit();
			
			$this->_endOperation();
			
			return true;
		}
		
		$this->_endOperation();
		
		return false;
		
	}
	
}