<?php

//pull in interfaces
require_once( 'interfaces/AsyncSessionCacheInterface.class.php' );
require_once( 'interfaces/AsyncSessionDatabaseInterface.class.php' );

//pull in the main class
require_once( 'classes/AsyncSession.class.php' );